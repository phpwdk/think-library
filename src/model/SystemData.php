<?php
declare (strict_types=1);

namespace think\simple\model;

use think\simple\Model;

/**
 * 系统数据模型
 * Class SystemData
 * @package think\simple\model
 */
class SystemData extends Model
{
}
