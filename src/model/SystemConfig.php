<?php
declare (strict_types=1);

namespace think\simple\model;

use think\simple\Model;

/**
 * 系统配置模型
 * Class SystemConfig
 * @package think\simple\model
 */
class SystemConfig extends Model
{
}
